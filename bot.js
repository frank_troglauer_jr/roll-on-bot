const Discord = require('discord.js');
const { prefix, prefix2, altprefix, token, token2 } = require('./config.json');

// Initialize bot
const client = new Discord.Client({
    token: token,
    autorun: true
});

client.on('ready', () => {
    console.log("Connected as " + client.user.tag)
    client.user.setActivity("ro! #")
})

client.login(token)

//Do something when message received
client.on('message', (receivedMessage) => {

    if(receivedMessage.author == client.user){
        return
    }
    //receivedMessage.channel.send("Message received: " + receivedMessage.content)

    if(receivedMessage.content.startsWith(prefix) || receivedMessage.content.startsWith(altprefix)){
        processCommand(receivedMessage)
    }
})

function processCommand(receivedMessage){
    let fullCommand = receivedMessage.content;
    let shortCommand = fullCommand.replace(prefix, "").replace(" ","");

    if(fullCommand.includes("help") || shortCommand.length == 0){
        receivedMessage.channel.send("Example Usage: `ro!5`, `ro! 20`, `ro! d20` `ro! 2d20`");
    } else 
    {
        let dice = [];
        if(shortCommand.includes("d"))
            dice = shortCommand.split("d");
        else
            dice = [1,shortCommand.replace(/\D/g,'')];

        console.log(prefix, shortCommand,dice);

        if(dice[1] < 1)
        {
            receivedMessage.channel.send(receivedMessage.author+", please use numbers > 0.");  
        }
        else if(isNaN(dice[1].replace(/\D/g,'')))
        {
            receivedMessage.channel.send(receivedMessage.author+", please use numbers! No math either, this is a simple dice rolling machine.");  
        } else if(dice.length > 0)
        {
            let result = 0;
            let resultfull = "";
            let num = (dice[0] != '' && !isNaN(dice[0])) ? dice[0] : 1;
            let diesize = dice[1].replace(/\D/g,'');
            let roll = 0;

            console.log(dice);

            for(var i = 0; i < num; i++)
            {
                roll = Math.floor(Math.random()*Math.floor(diesize))+1;
                if(i>0) resultfull+= "+";
                resultfull += roll;
                result+=roll;
            }
            resultfull += "="+result;

            console.log(result, resultfull);
            
            if(fullCommand.includes("d") && dice[0] > 1)
                receivedMessage.channel.send(receivedMessage.author+" rolled a "+resultfull+"!");
            else 
                receivedMessage.channel.send(receivedMessage.author+" rolled a "+result+"!");  

        } else {
            receivedMessage.channel.send("The command wasn't recognized. Please try again or check help using `ro! help`");            
        }
    }

}




